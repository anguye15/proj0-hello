# Proj0-Hello
-------------

Trivial project to exercise version control, turn-in, and other
mechanisms.

# Author:
---------------

Anthony Nguyen

# Contact Address:
---------------

anguye15@uoregon.edu

# Brief Description:
---------------

This software takes the source file (hello.py), and prints out a message.
The message is determined by the contents of credentials.ini .
